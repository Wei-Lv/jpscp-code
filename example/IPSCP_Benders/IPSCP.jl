include("startup.jl")
include("ReadData.jl")
include("OriModel.jl")
include("BendersModel.jl")

# method of calling: julia IPSCP.jl dataname numscenario datatype L epsilon method rootorchild timeLimits
#                    isHeur=(false true) isstren=(false true) isPre=(false true) isMIR=(false true) isInit=(false true) isAuto=(false true) isZBin=(false true)  
# example: julia IPSCP_Benders/IPSCP.jl scp47-50-0.gz 50 0 0 0.1 0 1 7200 isHeur=false isStren=false isPre=false isMIR=false isInit=false isAuto=false isZBin=false

const dataname = ARGS[1]
const numscenario = parse(Int, ARGS[2])
# independent data or correlate data: (0 1)
const datatype = parse(Int, ARGS[3])
# L=(0, 25, 50)
const L = parse(Int, ARGS[4])
const epsilon = parse(Float64, ARGS[5])
const method = parse(Int, ARGS[6])
# 是否只在根节点加 UserCut (0: root，1：all)
const rootorchild = parse(Int, ARGS[7])
const timeLimits =parse(Int, ARGS[8])
# const gap = 0.0
const EPS = 1e-4
const isOneTol = 1e-6
const solver = "Cplex"

params = Dict("isHeur"=>false, "isStren"=>false, "isPre"=> false, 
            "isMIR"=>false, "isInit" => false, "isAuto" => false, "isZBin" => false)

if length(ARGS) >= 9
    for param in ARGS[9:end]
        paramName, paramVal = split(param, '=', keepempty=false)
        if occursin("is", paramName)
            paramVal = paramVal == "true" ? true : false
        end
        params[paramName] = paramVal
    end
end


println("julia $(dataname) $numscenario $datatype $L $epsilon $method $rootorchild isPre=$(params["isPre"]) isHeur=$(params["isHeur"]) isStren=$(params["isStren"]) isMIR=$(params["isMIR"]) isInit=$(params["isInit"]) isAuto=$(params["isAuto"]) isZBin=$(params["isZBin"])")

function SolveIPSCP()
    # 获取数据
    @time omega, m, n, possibility, cost, A, numA = readIPSCP(dataname)
    # 随机矩阵规模
    println("m: $(m); n: $(n)")

    if method == 0
        println("Solve Origin Model:")
        OriModel(omega, m, n, possibility, cost, A, epsilon, params["isAuto"], params["isZBin"]; timeLimits=timeLimits, gap=0.0)
    else
        println("Solve Benders Model:")
        _, _, _, = BendersModel(omega, m, n, possibility, cost, A, numA, epsilon;
        isHeur=params["isHeur"], isStren=params["isStren"], isPre=params["isPre"], isMIR=params["isMIR"], isInit=params["isInit"], FixedSites=nothing, isOri=true, gap=0.0, timelimits=timeLimits)
    end
end

SolveIPSCP()

println("PS: ")