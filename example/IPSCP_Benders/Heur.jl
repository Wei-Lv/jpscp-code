"""
	Heuristic(RENS): Relaxation enforced neighborhood search	
"""
function HeurCB(cb_data, model, x, outer_start, flag, omega, m, n, possibility, cost, A, epsilon, numA, RecordX; timelimits=300)
  	if flag == 0
    	return flag
  	end

	nodecount = Ref{CPXLONG}()
	status = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODECOUNT, nodecount)
	@assert status == 0

    if flag == 1 && nodecount[] != 0
		flag = 0
		start = time()

		# why multiply 3 ? 
		timelimits = max(3*(start-outer_start), timelimits)

		# atol_lp (theta in paper) can be changed, such as 0.01 => 0 		
		atol_lp = 0.01
		# based on LP relaxation, fixing some variables to zero and one, respectively 
		lp_zeros = [j for j in 1:n if RecordX["x"][j] < atol_lp]
		lp_ones  = [j for j in 1:n if RecordX["x"][j] > 1 - atol_lp]

		# Key 1 corresponds to the value lp_ones, key 0 corresponds to the value lp_zeros
		fixed_sites = Dict(0=>lp_zeros, 1=>lp_ones)
		println("lp_ones: ",  length(lp_ones))
		println("lp_zeros: ", length(lp_zeros))

		duration = time()-start

		hstatus, sol_x_opt, obj = BendersModel(omega, m, n, possibility, cost, A, numA, epsilon; isHeur=false, isPrint=false, isStren=true, isMIR=true, isInit=true, isPre=false, FixedSites=fixed_sites, isOri=false, gap=0.0001, timelimits=max(0, timelimits-duration), prefix="heur")
		println("[heur] hstatus: ", hstatus)
		
		if hstatus == FEASIBLE_POINT 
			xfea = sol_x_opt
			MOI.submit(model, MOI.HeuristicSolution(cb_data), x, round.(xfea))
		end
		println("[heur] Heur obj: ", round(Int, obj))

		duration = time()-start
		println("[heur] Heurtime duration: ", round(duration, digits=2))
    else
		# get the LP relaxation at root node
		xValues = callback_value.(cb_data, x)
		RecordX["x"] .= xValues
    end

    return flag
 end