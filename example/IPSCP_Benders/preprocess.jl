function computeMod32(A, numA, m, omega)
	sortA = zeros(Int, m, omega)
	rowValues = zeros(UInt32, omega, m)
    for w in 1:omega
		sortA[:, w] = sortperm(numA[w, :], rev=true)
		for i in 1:m 
			ome = 0
			for k in A[w][i]
				temp = k & 31
				ome |= 1 << temp
			end
			rowValues[w, i] = ome
		end
	end
	return rowValues, sortA
end

function exactCompare(w1, i1, w2, i2, A, numA)
	flag = false
	ind = 1
	for j in A[w1][i1]
		flag = false
		for k in ind:numA[w2, i2]
			if j < A[w2][i2][k]
				return false
			elseif j == A[w2][i2][k]
				flag = true
				ind = k + 1
				break
			end
		end
	end
	return flag
end

function propagation3(m, omega, ScaledP, ScaledEps, A, numA)
	numlimit = omega - ScaledEps
	isSatisfied = zeros(Int8, m)
	isBound = zeros(Int8, omega, m)
	const_coef = zeros(m)
	rowValues, _ = computeMod32(A, numA, m, omega)
	sortW = zeros(Int, omega, m)
	for i in 1:m
		sortW[:, i] = sortperm(numA[:, i], rev=true)
	end
	rhs = omega - ScaledEps
	bound = omega - ScaledEps - ScaledP[1, 1]
	omegaPos = zeros(Int, omega, m)
	for i1 in 1:m
		GreatW = Int32[]
		for w1 in 1:omega
			w1pos = sortW[w1, i1]
			sumrhs = omega - ScaledP[w1pos, i1]
			for w2 in omega:-1:(w1+1)
				w2pos = sortW[w2, i1]
				if (rowValues[w1pos, i1] | rowValues[w2pos, i1]) == rowValues[w1pos, i1]
					isSubset = exactCompare(w2pos, i1, w1pos, i1, A, numA)
					if isSubset
						sumrhs -= ScaledP[w2pos, i1]
						if sumrhs - rhs < -isOneTol && omegaPos[w1pos, i1] == 0
							omegaPos[w1pos, i1] = 1
							isBound[w1pos, i1] = 1
							append!(GreatW, w1pos)
						elseif sumrhs - bound < -isOneTol
							isBound[w1pos, i1] = 0
							break
						end
					end
				end
			end
		end

		numw = length(GreatW)

		# 是否满足 ｜{w: A^w_i >= 1}｜ >= m - k
		if numw >= numlimit
			isSatisfied[i1] = 1
		end

		if numw > 0
			for w in numw:-1:1
				wpos = GreatW[w]
				if omegaPos[wpos, i1] == 2
					continue
				end
				for w1 in 1:(w-1)
					w1pos = GreatW[w1]
					if omegaPos[w1pos, i1] == 2
						continue
					end
					if (rowValues[wpos, i1] | rowValues[w1pos, i1]) == rowValues[w1pos, i1]
						isSubset = exactCompare(wpos, i1, w1pos, i1, A, numA)
						if isSubset
							omegaPos[w1pos, i1] = 2
						end
					end
				end
			end
		end
	end

	KeepOmega = [Int[] for i in 1:m]
	DelOmega = [Int[] for i in 1:m]
	BasicCons = [Int[] for i in 1:m]
	Boundary = [Int[] for i in 1:m]
	Ndelnum = 0
	for i in 1:m
		for w in 1:omega
			if omegaPos[w, i] == 1
				append!(BasicCons[i], w)
			end
			if isBound[w, i] == 1
				append!(Boundary[i], w)
			end
			if omegaPos[w, i] == 1 || omegaPos[w, i] == 2
				if isBound[w, i] == 1
					append!(KeepOmega[i], w)
					continue
				end
				append!(DelOmega[i], w)
				Ndelnum += 1
			elseif omegaPos[w, i] == 0
				append!(KeepOmega[i], w)
			end
		end
	end

	for i in 1:m
		for w in DelOmega[i]
			const_coef[i] += ScaledP[w, i]
		end
	end
	println("number of DelRows: ", sum(length(DelOmega[i]) for i in 1:m) )
	println("number of BasicCons: ", sum(length(BasicCons[i]) for i in 1:m) )
	println("number of KeepRows: ", m*omega - Ndelnum)
	return DelOmega, KeepOmega, BasicCons, const_coef, isSatisfied, Boundary
end
