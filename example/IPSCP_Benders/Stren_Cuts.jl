function StrenFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, isLazy, cutInfo, nodecount, UnsatisfiedM, KeepOmega, ConsCoef)
    
    start = time()
    # count the number of coefficient strengthening feasibility constraints added in the round
    nStrCons = 0

    # get relaxation solution
    xValues = callback_value.(cb_data, x)

    sumRows = zeros(Float64, omega * m)
    # for all scenarion, based on the relaxation solution, we compute the sum of each row of matrix
    ComputeRows(xValues, AbyColumn, sumRows, nodecount)
    
    @inbounds begin
        for i in UnsatisfiedM
            # str_lhs = 0.0
            str_constant = 0

            str_coeff = zeros(Int, n)
            preind = (i - 1) * omega
            for w in KeepOmega[i]
                ind = preind + w
                if sumRows[ind] < 1 - isOneTol
                    # str_lhs += ScaledP[w, i] * sumRows[ind]
                    str_coeff[A[w][i]] .+= ScaledP[w, i]
                else
                    str_constant += ScaledP[w, i] 
                end
            end
            str_rhs::Int = omega - ScaledEps - str_constant - ConsCoef[i]
            if str_rhs::Int <= 0
                continue
            end

            # coefficient strengthening
            for k in 1:n
                str_coeff[k] = min(str_rhs, str_coeff[k])
            end
            
            # calculating the Euclidean distance by the relaxation solution
            violated_val = EulerDist(str_coeff, str_rhs, xValues)
            if violated_val < -EPS::Float64
                nStrCons += 1
                StrenFeaCons = @build_constraint(str_coeff' * x >= str_rhs)
                # judge (Lazy Constraint/ User Cut)
                Cons = isLazy == 1 ? MOI.LazyConstraint : MOI.UserCut
                # add a violated feasibility constraint to the solver
                MOI.submit(model, Cons(cb_data), StrenFeaCons)
            end
        end
    end

    if isLazy == 1
        cutInfo["lazy"] += (time()-start)
        cutInfo["nlazy"] += 1
    else
        cutInfo["user"] += (time()-start)
        cutInfo["nuser"] += 1
    end

    return nStrCons
end
