function StrenMIRFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, isLazy, cutInfo, nodecount, UnsatisfiedM, KeepOmega, ConsCoef)
    
    start = time()
    nStrCons = 0
    nMIRCons = 0
    
    # get relaxation solution
    xValues = callback_value.(cb_data, x)

    sumRows = zeros(Float64, omega * m)
    # for all scenarion, based on the relaxation solution, we compute the sum of each row of matrix
    ComputeRows(xValues, AbyColumn, sumRows, nodecount)

    @inbounds begin
        xStatus = zeros(Int8, n)
        cStatus = zeros(Int8, n)
        for j in 1:n
            if xValues[j] >= 0.5
                xStatus[j] = 1
            end
            if 0.0 < xValues[j] < 1.0
                cStatus[j] = 1
            end
        end

        for i in UnsatisfiedM
            # str_lhs = 0.0
            str_constant = 0

            # compute coefficient and rhs
            str_coeff = zeros(Int, n)
            preind = (i - 1) * omega
            for w in KeepOmega[i]
                ind = preind + w
                if sumRows[ind] < 1 - isOneTol
                    # str_lhs += (ScaledP[w, i] * sumRows[ind])
                    str_coeff[A[w][i]] .+= ScaledP[w, i]
                else
                    str_constant += ScaledP[w, i]
                end
            end
            str_rhs::Int = omega - ScaledEps - str_constant - ConsCoef[i]
            if str_rhs::Int <= 0
                continue
            end

            # coefficient strengthening
            step2Str_coeff = zeros(Int, n)
            sumLessRhs::Int = 0
            for j in 1:n
                if str_coeff[j] >= str_rhs
                    str_coeff[j] = str_rhs
                    step2Str_coeff[j] = str_rhs
                else
                    sumLessRhs += str_coeff[j]
                end
            end
            
            # determine whether a second type of strengthening cut can be added
            if sumLessRhs < str_rhs
                # calculating the Euclidean distance by the relaxation solution and strengthening coefficient
                step2VioVal = EulerDist(step2Str_coeff, str_rhs, xValues)

                if step2VioVal < -EPS
                    # Directly add a second type of enhanced cut
                    nStrCons += 1

                    nCurStrCons = @build_constraint(step2Str_coeff' * x >= str_rhs)
                    # judge (Lazy Constraint/User Cut)
                    Cons = isLazy == 1 ? MOI.LazyConstraint : MOI.UserCut
                    MOI.submit(model, Cons(cb_data), nCurStrCons)
                end
                # disregarding the MIR:
                # The reason is that the constraint is facet and if it is not violated, the following MIR must not be violated
                continue
            end
            
            MIRFlag = 0
            mirTime = time()
            # calculating the Euclidean distance by the relaxation solution and strengthening coefficient
            violVal = EulerDist(str_coeff, str_rhs, xValues)
            # set filters
            if violVal < 0.1 || nodecount < 1
                if sum(cStatus) != 0
                    # division of sets according to coefficients and relaxation solution
                    setF = []
                    setL = []
                    setU = []
                    for j in 1:n
                        if str_coeff[j] >= isOneTol
                            if xStatus[j] == 0
                                append!(setL, j)
                            else
                                append!(setU, j)
                            end
                            if cStatus[j] == 1
                                append!(setF, j)
                            end
                        end
                    end

                    # the difference between the left and right ends of the MIR inequality is recorded to find the most violated cut
                    most_viol_value = 0.0

                    coef_L = zeros(Int, n)
                    coef_U = zeros(Int, n)
                    coef_con::Int = 0
                    """
                    The form of MIR is
                        sum_{j in L} (r * K_j + min{r_j, r}) x_j + sum_{j in U} (r * K_j + min{r_j, r}) (1 - x_j) >= r * (K + 1),
                    where K_j = floor(C_j/delta) for j in L, K_j = floor(-C_j/delta) for j in U, r_j = C_j (mod delta) for j in {U, L},
                    r = beta (mod delta), and K = floor(beta/delta).
                    """
                    sum_U = length(setU) > 0 ? sum(str_coeff[j] for j in setU) : 0

                    for ind in setF
                        sum_rhs::Int = str_rhs-sum_U
                        FactorK = floor(Int, sum_rhs/str_coeff[ind])
                        RemainderR = mod(sum_rhs, str_coeff[ind])
                        if RemainderR == 0
                            continue
                        end

                        sumL = zeros(Int, n)
                        sumU = zeros(Int, n)
                        cur_con = RemainderR * (FactorK + 1)

                        sumL[setL] .= G.(str_coeff[setL], str_coeff[ind], RemainderR)
                        sumU[setU] .= G.(-str_coeff[setU], str_coeff[ind], RemainderR)

                        cur_con -= sum(sumU)
                        cur_viol = EulerDist(sumL-sumU, cur_con, xValues)
                        if cur_viol < most_viol_value
                            coef_L = sumL
                            coef_U = sumU
                            coef_con = cur_con
                            most_viol_value = cur_viol
                        end
                    end
                    
                    if most_viol_value < -EPS
                        # add MIR only if better than strengthening cut violation
                        if most_viol_value < violVal
                            MIRFlag = 1
                            nMIRCons += 1
                            nCurMIRCons = @build_constraint( (coef_L - coef_U )' * x >= coef_con)
                            # judge (Lazy Constraint/ User Cut)
                            Cons = isLazy == 1 ? MOI.LazyConstraint : MOI.UserCut
                            MOI.submit(model, Cons(cb_data), nCurMIRCons)
                        end
                    end
                end
            end
            cutInfo["mir"] += (time()-mirTime)

            if MIRFlag == 0 && violVal < -EPS
                nStrCons += 1
                nCurStrCons = @build_constraint(str_coeff' * x >= str_rhs)
                # judge (Lazy Constraint/ User Cut)
                Cons = isLazy == 1 ? MOI.LazyConstraint : MOI.UserCut
                MOI.submit(model, Cons(cb_data), nCurStrCons)
            end
        end
    end

    if isLazy == 1
        cutInfo["lazy"] += (time()-start)
        cutInfo["nlazy"] += 1
    else
        cutInfo["user"] += (time()-start)
        cutInfo["nuser"] += 1
    end

    return nStrCons, nMIRCons
end
