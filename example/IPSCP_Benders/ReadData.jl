using CodecZlib

function readIPSCP(filename) #filename = scp41-50-0.gz
    io = open(filename, "r") #打开指定文件 filename 以进行读取（"r" 表示只读模式）。open 函数返回一个文件对象，该对象赋值给变量 io
    gzip = GzipDecompressorStream(io) #使用 GzipDecompressorStream 创建一个 Gzip 解压缩流，该流用于解压缩从文件读取的内容。这个解压缩流会接收从文件中读取的压缩数据，并解压缩成原始数据
    io = readlines(gzip) 
    # io = readlines(filename)
    str = split(io[1], (' ', '\t', '\n'), keepempty=false)
    omega = parse(Int32, str[1])
    m = parse(Int32, str[2])
    n = parse(Int32, str[3])
    pnum = Int(ceil(omega * m / 20) + 1)
    # 记录 p^omega_i 的位置
    pos = 0
    possibility = zeros(omega, m)
    for i in 2:pnum
        str = split(io[i], (' ', '\t', '\n'), keepempty=false)
        for p in str
            pos += 1
            possibility[Int(ceil(pos/m)), pos % m == 0 ? m : pos % m] = parse(Float64, p)
        end
    end
    # 记录 cost
    cost = Vector{Int32}()
    for i in pnum+1:(pnum + Int(ceil(n/20)))
        str = split(io[i], (' ', '\t', '\n'), keepempty=false)
        for c in str
            append!(cost, parse(Int32, c))
        end
    end
    pnum += Int(ceil(n/20))
    A = Vector{Dict{Int, Vector{Int}}}(undef, omega)
    numA = zeros(Int, omega, m)
    for o in 1:omega
        A[o] = Dict()
        for i in 1:m
            num = parse(Int32, io[pnum + 1])
            numA[o, i] = num
            pnum += 1
            Record = []
            for row in 1:Int(ceil(num/20))
                str = split(io[pnum + 1], (' ', '\t', '\n'), keepempty=false)
                pnum +=1
                append!(Record, map(x->parse(Int32, x), str))
            end
            A[o][i] = Record
        end
    end

    return omega, m, n, possibility, cost, A, numA
end

if abspath(PROGRAM_FILE) == @__FILE__    
    @time omega, m, n, possibility, cost, A, numA, AbyColumn = readIPSCP(ARGS[1]) #ARGS[1] = scp41-50-0.gz
    println("omega: ", omega)
    println("m: ", m)
    println("n: ", n)
    println("possibility: ", possibility[20, 10])
    println("cost: ", cost[44])
    println("A: ", A[50][200])
    println("numA: ", numA[50, 200])
    println("AbyColumn: ", AbyColumn[1])
end
