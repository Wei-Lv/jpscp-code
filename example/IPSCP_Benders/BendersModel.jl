include("Auxiliary.jl")
include("preprocess.jl")
include("Heur.jl")
include("Basic_Cuts.jl")
include("Stren_Cuts.jl")
include("StrenCuts_MIR.jl")

function BendersModel(omega, m, n, possibility, cost, A, numA, epsilon;
    isHeur=false, isPrint=true, isStren=false, isMIR=false, isPre=false, isInit=false, FixedSites=nothing, isOri=true, gap=0.0, timelimits=7200, prefix="main")

    startbeforebuildmodel = time()

    model = SeleteSolver(solver)
    # set timelimits, gap
    set_time_limit_sec(model, timelimits)
    set_optimizer_attribute(model, "CPXPARAM_MIP_Tolerances_MIPGap", gap)

    # bulid 0-1 variables x
    @variable(model, x[j in 1:n], Bin)
    # objective function: minimize c^T x
    @objective(model, Min, cost' * x)

    start = time()

    # scaling
    ScaledP = zeros(Int, omega, m)
    for w in 1:omega, i in 1:m
        ScaledP[w, i] = round(Int, possibility[w, i] * omega)
    end
    ScaledEps = floor(Int, epsilon * omega)

    # presolving
    DelOmega, BasicCons, ConsCoef, isSatisfied = nothing, nothing, zeros(m), zeros(Int8, m)
    KeepOmega = [[o for o in 1:omega] for i in 1:m]
    if isOri && isPre
        DelOmega, KeepOmega, BasicCons, ConsCoef, isSatisfied, Boundary = propagation3(m, omega, ScaledP, ScaledEps, A, numA)
        println("Number of Satisfied: ", sum(isSatisfied))
    end

    # statistics presolving time
    presolvetime = time()-start

    # add initial cuts sum_{omega in S} (p_i^omega*A_i^omega x) >= omega-ScaledEps_i forall i in [m]
    if isInit
        if !isPre
            @constraint(model, Initailcons[i in 1:m], sum(ScaledP[w, i]*sum(x[A[w][i]]) for w in 1:omega) >= omega-ScaledEps)
        else
            for i in 1:m
                @constraint(model, sum(ScaledP[w, i]*sum(x[A[w][i]]) for w in KeepOmega[i]) >= omega-ScaledEps-ConsCoef[i])
            end
        end
    end

    # random matrix column storage
    AbyColumn = ComputeAbyColumn(A, numA, omega, m, n, isPre, DelOmega)
    UnsatisfiedM = [i for i in 1:m if isSatisfied[i] == 0]

    # count the number of add cons
    nFeaconslazy = 0
    nFeaconsuser = 0
    nStrConslazy = 0
    nStrConsuser = 0
    nMIRConslazy = 0
    nMIRConsuser = 0
    # statistics time spent on lazy and user processes
    timeBDLazy = 0.
    timeBDUser = 0.
    # statistics cut information
    cutInfo = Dict("lazy" => 0., "user" => 0., "nlazy" => 0, "nuser" => 0, "mir"=>0.0)
    # record node information
    NodeInfo = Dict("LastDepth" => 0, "nCall" => 0, "LastNode" => 0)

    """
    Call the callback function of Cplex to dynamically add the violated feasibility constraint
    Constraints required to ensure that the model is solved correctly.
    Add Lazy Constraint, Used to eliminate some infeasible integer solutions.
    """
    function LazyConstraint(cb_data)
        lazyConsTimeStart = time()

        nodecount = Ref{CPXLONG}()
        status = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODECOUNT, nodecount)
        @assert status == 0
        
        if !isStren && !isMIR
            nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 1, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
            nFeaconslazy += nCurFeaCons
        elseif isStren && !isMIR
            nCurStrCons = StrenFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 1, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
            nStrConslazy += nCurStrCons
        elseif isStren && isMIR
            nCurStrCons, nCurMIRCons = StrenMIRFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 1, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
            nStrConslazy += nCurStrCons
            nMIRConslazy += nCurMIRCons
        end
        
        timeBDLazy += (time()-lazyConsTimeStart)
    end
    MOI.set(model, MOI.LazyConstraintCallback(), LazyConstraint)

    """
    Cutting out some non-integer solutions, Compact model and accelerate problem solving
    """
    function UserCut(cb_data)

        userCutsTimeStart = time()

        # get the current node number 
        nodecount = Ref{CPXLONG}()
        status = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODECOUNT, nodecount)

        if nodecount[] == NodeInfo["LastNode"]
            NodeInfo["nCall"] += 1
        else
            NodeInfo["LastNode"] = nodecount[]
            NodeInfo["nCall"] = 1
        end

        # get the current node depth 
        nodedepth = Ref{CPXLONG}()
        res = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODEDEPTH, nodedepth)

        # add User cut restriction: (i) only two entries are allowed at child nodes (ii) depth frequency is 10
        if (nodecount[] < 1 || NodeInfo["nCall"] <= 2) && (nodedepth[] <= 10 || nodedepth[] == NodeInfo["LastDepth"] + 10)
            # record node depth
            NodeInfo["LastDepth"] = nodedepth[]
            
            if rootorchild == 0
                if nodecount[] == 0
                    if !isStren && !isMIR
                        nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                        nFeaconsuser += nCurFeaCons
                    elseif isStren && !isMIR
                        nCurStrCons = StrenFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                        nStrConsuser += nCurStrCons
                    elseif isStren && isMIR
                        nCurStrCons, nCurMIRCons = StrenMIRFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                        nStrConsuser += nCurStrCons
                        nMIRConsuser += nCurMIRCons
                    end
                end
            else
                if !isStren && !isMIR
                    nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                    nFeaconsuser += nCurFeaCons
                elseif isStren && !isMIR
                    nCurStrCons = StrenFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                    nStrConsuser += nCurStrCons
                elseif isStren && isMIR
                    nCurStrCons, nCurMIRCons = StrenMIRFeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                    nStrConsuser += nCurStrCons
                    nMIRConsuser += nCurMIRCons
                end 
            end
        end
        
        timeBDUser += (time() - userCutsTimeStart)
    end
    MOI.set(model, MOI.UserCutCallback(), UserCut)

    # heuristic fixes the variables of the LP relaxation of take values of 0 and 1
    if FixedSites !== nothing
        @constraint(model, x0[j in FixedSites[0]], x[j] == 0)
        @constraint(model, x1[j in FixedSites[1]], x[j] == 1)
    end

    if isHeur && isOri
        # flag: Ensure that the heuristic is only performed once
        flag = 1
        # record the LP relaxation of root node
        RecordX = Dict("x" => zeros(n))

        function HeurCallBack(cb_data)
            flag = HeurCB(cb_data, model, x, startbeforebuildmodel, flag, omega, m, n, possibility, cost, A, epsilon, numA, RecordX; timelimits=300)
        end
        MOI.set(model, MOI.HeuristicCallback(), HeurCallBack)
    end

    optimize!(model)
    if isPrint
        # solving information
        PrintInfos(model; prefix=prefix)
        PrintInfosBD(model; prefix=prefix)

        # presolving information
        @printf("[%s] %s %.2f\n", prefix, "Presolving time: ", presolvetime)

        # adding cut number information
        @printf("[%s] %s %d\n", prefix, "Number of basci_cons lazy Cons: ", nFeaconslazy)
        @printf("[%s] %s %d\n", prefix, "Number of basci_cons user Cons: ", nFeaconsuser)
        @printf("[%s] %s %d\n", prefix, "Number of Stren lazy Cons: ", nStrConslazy)
        @printf("[%s] %s %d\n", prefix, "Number of Stren user Cons: ", nStrConsuser)
        @printf("[%s] %s %d\n", prefix, "Number of lazy MIR: ", nMIRConslazy)
        @printf("[%s] %s %d\n", prefix, "Number of user MIR: ", nMIRConsuser)
        @printf("[%s] %s %d\n", prefix, "Number of str_mir user Cons: ", nStrConsuser+nMIRConsuser)
        
        # add cut time information
        @printf("[%s] %s %.2f\n", prefix, "MIR spent time: ", cutInfo["mir"])
        @printf("[%s] %s %.2f %s %d\n", prefix, "Lazy func time: ", cutInfo["lazy"], "Lazy func num: ", cutInfo["nlazy"])
        @printf("[%s] %s %.2f %s %d\n", prefix, "User func time: ", cutInfo["user"], "User func num: ", cutInfo["nuser"])
        @printf("[%s] %s %.2f\n", prefix, "AddBDCuts time: ", timeBDUser+timeBDLazy)
    end

    # get the optimal solution and value for heuristic
    pstatus = primal_status(model)
    sol_x = []
    objvalue = Inf
    if pstatus == FEASIBLE_POINT
        sol_x = value.(x)
        objvalue = objective_value(model)
    end

    return pstatus, sol_x, objvalue
end
