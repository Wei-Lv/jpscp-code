# p 为生出数据时最大消失概率
p=(0.4)
# scene 由外部传入，代表场景数
scene=$1

for (( i=0; i<1; i++ ))
do
    # 生成数据
    cd data
    if [ ! -d Scp$scene-${p[i]}-2 ];then
        mkdir Scp$scene-${p[i]}-2
    fi
    
    cd Scp$scene-${p[i]}-2

    rm *

    ../../newdataipscp/indep_py_sh/Gen_indep.sh ${p[i]} $scene

    ../../newdataipscp/corre_py_sh/Gen_corre.sh ${p[i]} $scene

    cd ../../
done
