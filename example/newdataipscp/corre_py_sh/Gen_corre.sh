#!/bin/bash

x1=julia
# x2=/share/home/zhangyuhang/ycy/setcoveringcode/newdataipscp/corre_py_sh/gendata_corr.jl
x2=/share/home/pq/lvwei/setcoveringcode/newdataipscp/corre_py_sh/gendata_corr.jl
# x2=/share/home/liangjie/setcoveringcode/newdataipscp/indep_py_sh/gendata_indep.jl
#names=/share/home/liangjie/setcoveringcode/newdataipscp/
# Filename=/share/home/liangjie/setcoveringcode/newdataipscp
# Filename=/share/home/liangjie/setcoveringcode/data
Filename=/share/home/pq/lvwei/setcoveringcode/data
# Filename=/share/home/zhangyuhang/ycy/setcoveringcode/data
dataname=(scp4 scp5 scp6 scpa scpb scpc scpd scpe scpnre scpnrf)
#if [ -f $File ]
#then
#  cat $File | while read line
#do 
#   tmp=$line
seed=60
for ((d=0; d<10; d++))
do
   if [ $d -lt 2 ];then
      num=10
   else
      num=5
   fi
      for ((i=1; i<=$num; i++))
      do
         names=${Filename}/${dataname[d]}/${dataname[d]}$i.txt
         # outname=${dataname[d]}$i
         # for j in 50 100 200 500 1000
         # $2 由外部传入的场景数， $1 由外部传入的概率 p
         for j in $2
         do
            for l in 50
            do
               seed=`expr $seed + 1`
               ${x1} ${x2} ${names} ${j} $l ${dataname[d]}${i}-${j}-$l $1 $seed
               gzip -c ${dataname[d]}${i}-${j}-$l > ${dataname[d]}${i}-${j}-$l.gz
               rm ${dataname[d]}${i}-${j}-$l
            done
         done
      done
done

