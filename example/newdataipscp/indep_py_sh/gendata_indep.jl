using Random
using Distributions

const sequence = [0, 1]

function num_certain_probability(probability)
    x = rand()
    cumulative_probability = 0.0
    for (item, itemp) in zip(sequence, probability)
        cumulative_probability += itemp
        if x <= cumulative_probability
            return item
        end
    end
end

# function generate_probabilistic_index(probability, m, n)
#     indicator_array = zeros(m, n)
#     for i in 1:m, j in 1:n
#         result = num_certain_probability([probability[i, j], 1 - probability[i, j]])
#         indicator_array[i, j] = result
#     end
#     return indicator_array
# end

function getdata(original, indicator, cons_size, pmax, vector_size)
    row_record = []
    index_record = []
    for i in 1:cons_size
        num = 0
        # for j in original[i]
        #     if indicator[i, j] == 1
        #         num += 1
        #         push!(index_record, j)
        #     end
        # end
        while num == 0
            probabilities = rand(Uniform(0, pmax), vector_size)
            for j in original[i]
                result = num_certain_probability([probabilities[j], 1 - probabilities[j]])
                if result == 1
                    num += 1
                    push!(index_record, j)
                end
            end
        end
        push!(row_record, num)
    end
    return row_record, index_record
end

function readdata(filename, rename, gen_size, seed, scenario, pmax)
    a = Int[]
    for line in eachline(filename)
        append!(a, parse.(Int, split(strip(line), (' '), keepempty=false)) )
    end
    cons_size = a[1]
    vector_size = a[2]
    original = []
    cost = a[3:(2 + vector_size)]
    position = 3 + vector_size
    record = Int[]
    for row in 1:cons_size
        count = a[position]
        push!(record, count)
        position += 1
        index = [x for x in a[position:(position + count - 1)]]
        push!(original, index)
        position += count
    end

    f = open(rename, "w")
    write(f, string(gen_size) * " " * string(cons_size) * " " * string(vector_size) * "\n")
    p = fill(1/gen_size, gen_size * cons_size)
    posp, len_p = 1, gen_size * cons_size
    while len_p > 20
        s = p[posp:(posp + 20 - 1)]
        s = join(string.(s), " ")
        write(f, s * "\n")
        posp += 20
        len_p -= 20
    end
    s = p[posp:(posp + len_p - 1)]
    s = join(string.(s), " ")
    write(f, s * "\n")
    
    posc, len_c = 1, length(cost)
    while len_c > 20
        s = cost[posc:(posc + 20 - 1)]
        s = join(string.(s), " ")
        write(f, s * "\n")
        posc += 20
        len_c -= 20
    end
    s = cost[posc:(posc + len_c - 1)]
    s = join(string.(s), " ")
    write(f, s * "\n")

    Random.seed!(seed)

    for i in 1:gen_size
        # probability = rand(Uniform(0, pmax), cons_size, vector_size)
        # indicator_matrix = generate_probabilistic_index(probability, cons_size, vector_size)
        # row_record, index_record = getdata(original, indicator_matrix, cons_size, pmax, vector_size)
        row_record, index_record = getdata(original, 0, cons_size, pmax, vector_size)
        pos = 1
        for item in 1:cons_size
            write(f, string(row_record[item]), "\n")
            len_idx = row_record[item]
            while len_idx > 20
                s = index_record[pos:(pos + 20 - 1)]
                s = join(string.(s), " ")
                write(f, s * "\n")
                pos += 20
                len_idx -= 20
            end
            s = index_record[pos:(pos + len_idx - 1)]
            s = join(string.(s), " ")
            write(f, s * "\n")
            pos += len_idx
        end
    end
    close(f)
    println("done!!!")
    return p, cost
end

if abspath(PROGRAM_FILE) == @__FILE__
    file = ARGS[1]
    gensize = parse(Int, ARGS[2])
    scenario = parse(Int, ARGS[3])
    rename = ARGS[4]
    pmax = parse(Float64, ARGS[5])
    seed = parse(Int, ARGS[6])
    println(pmax)
    @time p, cost= readdata(file, rename, gensize, seed, scenario, pmax)
    # println(p)
    # println(cost)
end