import numpy as np
import sys
import random


def num_certain_probability(sequence: list, probability: list):
    x = random.uniform(0, 1)
    cumulative_probability = 0.0
    for item, itemp in zip(sequence, probability):
        # print("item", item)
        # print(itemp)
        cumulative_probability += itemp
        if x <= cumulative_probability:
            return item


def generate_probabilistic_index(sequence: list, probability: list, m: int, n: int):
    indicator_array = np.zeros((m, n))
    count = {0: 0, 1: 0}
    for i in range(m):
        for j in range(n):
            result = num_certain_probability(sequence, [probability[i][j], 1 - probability[i][j]])
            indicator_array[i, j] = result
            count[result] += 1
    # print(indicator_array)
    return indicator_array


def generate_probability_matrixs(original, indicator_ndarray):
    new_matrix = original * indicator_ndarray
    return new_matrix


def getdata(np_array, cons_size):
    row_record = []
    index_record = []
    for i in range(cons_size):
        index = np.argwhere(np_array[i] == 1)
        count = len(index)
        row_record.append(count)
        for item in range(count):
            index_record.append(int(index[item]))
    return row_record, index_record

def randpiw(cons_size:int, gen_size:int):
    pio = []
    pio1 = []
    for i in range(cons_size):
        p = [np.random.randint(1, 100) for i in range(gen_size - 1)]
        p.append(0)
        p.append(100)
        p.sort()
        p1 = [p[i+1] - p[i] for i in range(gen_size)]
        # print("p1", p1)
        for w in range(gen_size):
            p1[w] = str(p1[w] / 100)
            pio.append(p1[w])
        # print(pio)
    for w in range(gen_size):
        for i in range(cons_size):
            pio1.append(pio[i * gen_size + w])
    # print(pio1)
    return pio1


def readdata(filename: str, renamefile: str, gen_size: int):
    with open(filename, 'r') as f:
        a = []
        for line in f.readlines():
            a = a + list(line.rstrip('\n').strip(' ').split(' '))
    f.close()
    a = [int(x) for x in a]
    # cons_size = m, vector_size = n
    cons_size = a[0]
    vector_size = a[1]
    original = np.zeros((cons_size, vector_size))
    cost = a[2:(2 + vector_size)]
    position = 2 + vector_size
    record = []
    for row in range(cons_size):
        count = a[position]
        record.append(count)
        position = position + 1
        index = [x - 1 for x in a[position:(position + count)]]
        original[row][index] = 1
        position = position + count
    with open(renamefile, 'w') as f:
        f.write(str(gen_size) + ' ' + str(cons_size) +
                ' ' + str(vector_size) + '\n')
        p = [str(1/gen_size) for x in range(gen_size * cons_size)]
        #p = randpiw(cons_size, gen_size)
        position_p = 0
        len_p = gen_size * cons_size
        # write p into file
        while len_p > 20:
            s = p[position_p:(position_p + 20)]
            s = ' '.join(s)
            f.write(s + '\n')
            position_p = position_p + 20
            len_p = len_p - 20
        s = p[position_p:(position_p + len_p)]
        s = ' '.join(s)
        f.write(s + '\n')
        l_cost = len(cost)
        position_cost = 0
        # write cost into file
        while l_cost > 20:
            s = cost[position_cost:(position_cost + 20)]
            s = [str(x) for x in s]
            s = ' '.join(s)
            f.write(s + '\n')
            position_cost = position_cost + 20
            l_cost = l_cost - 20
        s = cost[position_cost:(position_cost + l_cost)]
        s = [str(x) for x in s]
        s = ' '.join(s)
        f.write(s + '\n')
        sequence = [0, 1]
        # the probability of column disappearance
        # probability = [0.1, 0.9]
        # indicator_rescord = []
        for i in range(gen_size):
            probability = []
            for k in range(cons_size):
                temp = []
                for j in range(vector_size):
                    temppro = np.random.uniform(0, 0.4)
                    temp.append(temppro)
                probability.append(temp)
            # print(probability)
            indicator_matrix = generate_probabilistic_index(
                sequence, probability, cons_size, vector_size)
            # avoid the same indicator array
            # while(indicator_array in indicator_record):
            #     indicator_array = generate_probabilistic_index(
            #         sequence, probability, vector_size)
            new_matrix = generate_probability_matrixs(
                original, indicator_matrix)
            row_record, index_record = getdata(new_matrix, cons_size)
            index_position = 0
            for item in range(cons_size):
                f.write(str(row_record[item]) + '\n')
                l_index_record = row_record[item]
                while l_index_record > 20:
                    s = index_record[index_position:(index_position + 20)]
                    s = [str(x+1) for x in s]
                    s = ' '.join(s)
                    f.write(s + '\n')
                    index_position = index_position + 20
                    l_index_record = l_index_record - 20
                s = index_record[index_position:(index_position + l_index_record)]
                s = [str(x+1) for x in s]
                s = ' '.join(s)
                f.write(s + '\n')
                index_position = index_position + l_index_record
    f.close()
    print("good job!!!")
    # print(cost)


if __name__ == '__main__':
    filename = sys.argv[1]
    renamefile = sys.argv[2]
    gen_size = int(sys.argv[3])
    import time
    a1 = time.time()
    readdata(filename, renamefile, gen_size)
    a2 = time.time()
    print(a2 - a1)
