using Random
using DelimitedFiles

function num_certain_probability(sequence::Vector{T}, probability::Vector{T}) where T
    x = rand(Float64)
    cumulative_probability = 0.0
    for (item, itemp) in zip(sequence, probability)
        cumulative_probability += itemp
        if x <= cumulative_probability
            return item
        end
    end
end

function generate_probabilistic_index(sequence::Vector{T}, probability::Matrix{T}, m::Int, n::Int) where T
    indicator_array = zeros(m, n)
    count = Dict(0 => 0, 1 => 0)
    for i in 1:m
        for j in 1:n
            result = num_certain_probability(sequence, [probability[i,j], 1 - probability[i,j]])
            indicator_array[i, j] = result
            count[result] += 1
        end
    end
    return indicator_array
end

function generate_probability_matrixs(original::Matrix{T}, indicator_ndarray::Matrix{T}) where T
    new_matrix = original .* indicator_ndarray
    return new_matrix
end

function getdata(np_array::Matrix{T}, cons_size::Int) where T
    row_record = []
    index_record = []
    for i in 1:cons_size
        row = np_array[i, :]
        index = findall(x -> x != 0, row)
        len_index = length(index)
        row_record.append(len_index)
        index_record = vcat(index_record, index)
    end
    return row_record, index_record
end

function readdata(filename::String, renamefile::String, gen_size::Int)
    a = readdlm(filename, ' ', Int)
    a = vec(a)
    cons_size = a[1]
    vector_size = a[2]
    original = zeros(cons_size, vector_size)
    cost = a[3:(2 + vector_size)]
    position = 2 + vector_size
    record = []
    for row in 1:cons_size
        count = a[position]
        push!(record, count)
        position += 1
        index = [x - 1 for x in a[position:(position + count - 1)]]
        original[row, index] = 1
        position += count
        open(renamefile, "w") do f
        write(f, string(gen_size) * " " * string(cons_size) * " " * string(vector_size) * "\n")
        p = repeat(string(1/gen_size), gen_size * cons_size)
        position_p = 1
        len_p = gen_size * cons_size
        while len_p > 20
            s = join(p[position_p:(position_p + 19)], " ")
            write(f, s * "\n")
            position_p += 20
            len_p -= 20
        end
        s = join(p[position_p:(position_p + len_p - 1)], " ")
        write(f, s * "\n")
        l_cost = length(cost)
        position_cost = 1
        while l_cost > 20
            s = join(string.(cost[position_cost:(position_cost + 19)]), " ")
            write(f, s * "\n")
            position_cost += 20
            l_cost -= 20
        end
        s = join(string.(cost[position_cost:(position_cost + l_cost - 1)]), " ")
        write(f, s * "\n")
        sequence = [0, 1]
        for i in 1:gen_size
            probability = rand(Float64, cons_size, vector_size) .* 0.2
            indicator_matrix = generate_probabilistic_index(sequence, probability, cons_size, vector_size)
            new_matrix = generate_probability_matrixs(original, indicator_matrix)
            row_record, index_record = getdata(new_matrix, cons_size)
            index_position = 1
            for item in row_record
                write(f, string(item) * "\n")
                l_index_record = item
                while l_index_record > 20
                    s = join(string.(index_record[index_position:(index_position + 19)] .+ 1), " ")
                    write(f, s * "\n")
                    index_position += 20
                    l_index_record -= 20
                end
                s = join(string.(index_record[index_position:(index_position + l_index_record - 1)] .+ 1), " ")
                write(f, s * "\n")
                index_position += l_index_record
            end
        end
    end
    println("good job!!!")
end

if abspath(PROGRAM_FILE) == @__FILE__
    filename = ARGS[1]
    renamefile = ARGS[2]
    gen_size = ARGS[3]
    readdata(filename,renamefile,gen_size)
end