#!/bin/bash

#尝试运行会报错：[xiangkeni@mgt 1-JPSCP]$ ./11-code/gendata/Gen_indep.sh 0.4 50  bash: ./11-code/gendata/Gen_indep.sh: Permission denied

x1=julia
x2=/share/home/xiangkeni/my-project/1-JPSCP/11-code/gendata/gendata_indep.jl
Filename=/share/home/xiangkeni/my-project/1-JPSCP/10-data
# Filename=/share/home/zhangyuhang/ycy/setcoveringcode/data
dataname=(scp4 scp5 scp6 scpa scpb scpc scpd scpe scpnre scpnrf)
#if [ -f $File ]
#then
#  cat $File | while read line
#do 
#   tmp=$line
seed=0
for ((d=0; d<10; d++))
do
   if [ $d -lt 2 ];then
      num=10
   else
      num=5
   fi
      for ((i=1; i<=$num; i++))
      do
         names=${Filename}/${dataname[d]}/${dataname[d]}$i.txt
         # outname=${dataname[d]}$i
         # for j in 50 100 200 500 1000
         # $2 由外部传入的场景数， $1 由外部传入的概率 p
         for j in $2
         do
            seed=`expr $seed + 1`
            ${x1} ${x2} ${names} ${j} 0 ${dataname[d]}${i}-${j} $1 $seed
            gzip -c ${dataname[d]}${i}-${j} > ${dataname[d]}${i}-${j}-0.gz
            rm ${dataname[d]}${i}-${j}
         done
      done
done
