#命令行应该是：TestSmallSize.sh 100 

# p 为生出数据时最大消失概率
p=(0.4) #定义了一个数组 p，其中包含一个元素 0.4。
# scene 由外部传入，代表场景数
scene=$1 

for (( i=0; i<1; i++ )) #函数只运行了一次
do
    # 生成数据
    cd 10-data
    if [ ! -d Scp$scene-${p[i]}-2 ];then #如果文件夹 Scp$scene-${p[i]}-2（Scp100-0.4-2） 不存在，执行以下操作
        mkdir Scp$scene-${p[i]}-2 #创建文件夹 `Scp$scene-${p[i]}-2`（Scp100-0.4-2）
    fi
    
    cd Scp$scene-${p[i]}-2

    rm * #删除当前目录下的所有文件

    ./11-code/gendata/Gen_indep.sh
    #../../newdataipscp/indep_py_sh/Gen_indep.sh ${p[i]} $scene #运行生成数据的脚本
    #../../newdataipscp/corre_py_sh/Gen_corre.sh ${p[i]} $scene #运行生成数据的脚本

    cd ../../ #切换回上一级目录（应该是data的上一级目录）。
done
