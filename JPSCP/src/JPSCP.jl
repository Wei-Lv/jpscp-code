include("startup.jl")
include("ReadData.jl")
include("OriModel.jl")
include("BendersModel.jl")

# method of calling: julia IPSCP.jl dataname numscenario datatype L epsilon method rootorchild timeLimits
#                    isHeur=(false true) isstren=(false true) isPre=(false true) isMIR=(false true) isInit=(false true) isAuto=(false true) isZBin=(false true)  
# example: julia JPSCP.jl scp41-50-0.gz 50 0 0 0.1 0 1 7200 isHeur=false isStren=false isPre=false isMIR=false isInit=false isAuto=false isZBin=false

const dataname = ARGS[1] #数据集：scp41-50-0.gz
const numscenario = parse(Int, ARGS[2]) #场景数：50
# independent data or correlate data: (0 1)
const datatype = parse(Int, ARGS[3]) #独立数据还是不独立数据：0
# L=(0, 25, 50)
const L = parse(Int, ARGS[4]) #？
const epsilon = parse(Float64, ARGS[5]) #约束不满足的最大概率：0.1
const method = parse(Int, ARGS[6]) #加割方法：0
# 是否只在根节点加 UserCut (0: root，1：all)
const rootorchild = parse(Int, ARGS[7]) #？
const timeLimits =parse(Int, ARGS[8]) #模型的时间限制：7200
# const gap = 0.0
const EPS = 1e-4 #
const isOneTol = 1e-6 #判断是不是零用的
const solver = "Cplex" #求解器设定

params = Dict("isHeur"=>false, "isStren"=>false, "isPre"=> false, 
            "isMIR"=>false, "isInit" => false, "isAuto" => false, "isZBin" => false) #默认一些参数设定

if length(ARGS) >= 9
    for param in ARGS[9:end]
        paramName, paramVal = split(param, '=', keepempty=false) #使用 = 将参数分割成参数名和参数值，这里 keepempty=false 表示不保留空字符串。
        if occursin("is", paramName) #检查参数名中是否包含字符串 "is"。
            paramVal = paramVal == "true" ? true : false #如果参数值是字符串 "true"，则将 paramVal 设置为 true，否则设置为 false。
        end
        params[paramName] = paramVal #将参数名和对应的值存储到之前提到的 params 字典中。
    end
end


println("julia $(dataname) $numscenario $datatype $L $epsilon $method $rootorchild isPre=$(params["isPre"]) isHeur=$(params["isHeur"]) isStren=$(params["isStren"]) isMIR=$(params["isMIR"]) isInit=$(params["isInit"]) isAuto=$(params["isAuto"]) isZBin=$(params["isZBin"])")

function SolveJPSCP() #文件主体
    # 获取数据
    @time omega, m, n, cost, A, numA = readIPSCP(dataname) #ReadData.jl中
    # 随机矩阵规模
    println("m: $(m); n: $(n)")

    if method == 0
        println("Solve Origin Model:")
        OriModel(omega, m, n, cost, A, epsilon, params["isAuto"], params["isZBin"]; timeLimits=timeLimits, gap=0.0) #OriModel.jl中
        #OriModel(omega, m, n, possibility, cost, A, epsilon, params["isAuto"], params["isZBin"]; timeLimits=timeLimits, gap=0.0)
    else
        println("Solve Benders Model:")
        #_, _, _, = BendersModel(omega, m, n, possibility, cost, A, numA, epsilon;
        _, _, _, = BendersModel(omega, m, n, cost, A, numA, epsilon; isPre=params["isPre"], isOri=true, gap=0.0, timelimits=timeLimits)
        # isHeur=params["isHeur"], isStren=params["isStren"], isPre=params["isPre"], isMIR=params["isMIR"], isInit=params["isInit"], FixedSites=nothing, isOri=true, gap=0.0, timelimits=timeLimits)
    end
end

SolveJPSCP()

println("PS: ")