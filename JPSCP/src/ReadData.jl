using CodecZlib

function readIPSCP(filename)#例子：readIPSCP(scp41-50-0.gz)
    io = open(filename, "r")#打开指定文件（由变量 filename 指定）并将文件句柄赋给变量 io。使用 "r" 表示以只读模式打开文件。
    gzip = GzipDecompressorStream(io)#创建一个 Gzip 解压缩流（GzipDecompressorStream），将前面打开的文件句柄 io 作为输入。这表明文件可能是以 gzip 格式压缩的，并且现在我们将对其进行解压缩。
    #解压结束gzip应该是一个txt文件
    io = readlines(gzip)#使用 readlines 函数从解压缩流中读取所有行，并将它们存储在变量 io 中。现在，io 是一个包含文件中每一行文本的数组。    
    str = split(io[1], (' ', '\t', '\n'), keepempty=false)#使用 split 函数将第一行文本（io[1]）按照空格、制表符和换行符进行分割。 keepempty=false 表示不保留空字符串。结果将存储在变量 str 中，它是一个包含分割后的字符串的数组。
    #文件的第一行是场景数、行数、列数：50 200 1000
    omega = parse(Int32, str[1])
    m = parse(Int32, str[2])
    n = parse(Int32, str[3])#使用 parse 函数将分割后的字符串转换为整数，并将它们赋给变量 omega、m 和 n。

    pnum = 1 + Int(ceil(n/20))
    # 记录 cost
    cost = Vector{Int32}()#创建了一个空的 Int32 类型的数组
    for i in 2:pnum
        str = split(io[i], (' ', '\t', '\n'), keepempty=false)
        for c in str
            append!(cost, parse(Int32, c))
        end
    end

    #下面开始读取支撑集的信息
    A = Vector{Dict{Int, Vector{Int}}}(undef, omega)#记录第Omega个场景下矩阵A的第m行的非零元素在哪几列。
    #Vector{Dict{Int, Vector{Int}}}: 这部分表示创建一个存储 Dict{Int, Vector{Int}} 类型元素的数组。在这里，Dict{Int, Vector{Int}} 表示存储整数键和整数数组值的字典。
    #(undef, omega): 这部分是构造函数调用的参数。undef 表示未初始化的值，而 omega 是数组的长度。因此，A 是一个包含 omega 个未初始化字典的数组。
    numA = zeros(Int, omega, m)#记录第Omega个场景下矩阵A的第m行有几个非零元素
    for o in 1:omega
        A[o] = Dict()#空字典
        for i in 1:m
            num = parse(Int32, io[pnum + 1])#使用 parse 函数将字符串 io[pnum + 1] 转换为 32 位整数 (Int32)。
            numA[o, i] = num 
            pnum += 1
            Record = []
            for row in 1:Int(ceil(num/20))
                str = split(io[pnum + 1], (' ', '\t', '\n'), keepempty=false)
                pnum +=1
                append!(Record, map(x->parse(Int32, x), str))
                #map(x->parse(Int32, x), str): 这部分使用 map 函数，对字符串数组 str 中的每个元素应用一个匿名函数。这个匿名函数使用 parse 函数将字符串转换为 32 位整数 (Int32)。
                #append!(Record, ...): 这是一个对数组进行追加操作的函数调用。append! 函数将第二个参数中的元素追加到第一个参数中。
            end
            A[o][i] = Record
        end
    end
    return omega, m, n, cost, A, numA
end

if abspath(PROGRAM_FILE) == @__FILE__    
    @time omega, m, n, cost, A, numA = readIPSCP(ARGS[1])
    println("omega: ", omega)
    println("m: ", m)
    println("n: ", n)
    println("cost: ", cost[44])
    println("A: ", A[50][200])
    println("numA: ", numA[50, 200])
end
