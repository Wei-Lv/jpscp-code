function FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, isLazy, cutInfo, nodecount, UnsatisfiedM, KeepOmega, ConsCoef)
    
    start = time()
    # count the number of feasibility constraints added in the round
    nCurFeaCons = 0
    
    # get relaxation solution
    xValues = callback_value.(cb_data, x)

    sumRows = zeros(Float64, omega * m)
    # for all scenarion, based on the relaxation solution, we compute the sum of each row of matrix
    ComputeRows(xValues, AbyColumn, sumRows, nodecount)
    
    # @inbounds begin-end combine multiple statements together to disable array bounds checking throughout the block to improve code performance
    @inbounds begin
        # basic_lhs = 0.0
        basic_constant = 0
        
        basic_coeff = zeros(Int, n)
        
        for w in 1:omega
            sumRows_w = zeros(m)
            # for i in UnsatisfiedM
            for i in 1:m
                preind = (i - 1) * omega
                ind = preind + w
                sumRows_w[i] = sumRows[ind]
            end
            min_value = minimum(sumRows_w)
            min_index = argmin(sumRows_w)

            if min_value < 1-isOneTol
                # basic_lhs += ScaledP[w, i] * sumRows[ind]
                basic_coeff[A[w][min_index]] .+= ScaledP[w, min_index]
            else
                basic_constant += ScaledP[w, min_index]
            end
        end

        basic_rhs::Int = omega - ScaledEps - basic_constant # - ConsCoef[i]
        if basic_rhs::Int > 0

            # calculating the Euclidean distance by the relaxation solution
            violated_val = EulerDist(basic_coeff, basic_rhs, xValues)
            if violated_val < -EPS::Float64
                nCurFeaCons += 1
                FeaCons = @build_constraint(basic_coeff' * x >= basic_rhs)
                # judge (Lazy Constraint/ User Cut)
                Cons = isLazy == 1 ? MOI.LazyConstraint : MOI.UserCut
                # add a violated feasibility constraint to the solver
                MOI.submit(model, Cons(cb_data), FeaCons)
            end

        end
    end

    if isLazy == 1
        cutInfo["lazy"] += (time()-start)
        cutInfo["nlazy"] += 1
    else
        cutInfo["user"] += (time()-start)
        cutInfo["nuser"] += 1
    end

    return nCurFeaCons
end
