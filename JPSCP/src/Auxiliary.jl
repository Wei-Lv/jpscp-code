
"""
    example: n = 4, m = 2, omega = 2,
		A = [ [ [1, 3], 		// (i-1) * omega + w = (1 - 1) * 2 + 1 = 1
				  [2, 4] ], 	// (i-1) * omega + w = (2 - 1) * 2 + 1 = 3
				[ [1, 2],		// (i-1) * omega + w = (1 - 1) * 2 + 2 = 2
				  [2, 3] ] ].	// (i-1) * omega + w = (2 - 1) * 2 + 2 = 4
		AbyColumn = [[1, 2], [2, 3, 4], [1, 4], [3]]
"""

function ComputeAbyColumn(A, numA, omega, m, n, isPre, DelOmega)
	AbyColumn = [Vector{Int64}() for j in 1:n]
	DelIndicate = zeros(Int8, omega, m)

	for i in 1:m
		if isPre
			DelIndicate[DelOmega[i], i] .= 1
		end
		numPreRows = (i-1)*omega
		for w in 1:omega
			if DelIndicate[w, i] == 0
				append!.(AbyColumn[A[w][i]], numPreRows+w)
			end
		end
	end

	return AbyColumn
end

"""
    for all scenarion, based on the relaxation solution, we compute the sum of each row of matrix
"""
# @inline: Used by the compiler to inline the code of a function into the place where it is called, to avoid the overhead of function calls
@inline function ComputeRows(xValues, AbyColumn, sumRows, nodecount)
	# The purpose of giving different violation degrees according to the nodes is to remove the cuts with smaller violations 
    # and thus reduce the number of prices
    if nodecount == 0
		indNZx = [j for j in 1:length(xValues) if xValues[j] > isOneTol]
	else
		indNZx = [j for j in 1:length(xValues) if xValues[j] > 0.1]
	end

    @inbounds begin
        for j in indNZx
            sumRows[AbyColumn[j]] .+= xValues[j]
        end
    end
end

"""
    Calculating the Euclidean distance by the relaxation solution
"""
@inline function EulerDist(vector_coef, const_coef, xValues)
    most_obey = (vector_coef' * xValues - const_coef) / sqrt(vector_coef' * vector_coef)
    return most_obey
end


@inline function G(Cj::Int, delta::Int, RemainderR::Int)
    FactorKj = floor(Int, Cj/delta)
    RemainderRj = mod(Cj, delta)
    return RemainderR * FactorKj + min(RemainderRj, RemainderR)
end