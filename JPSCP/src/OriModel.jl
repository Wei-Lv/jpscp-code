#OriModel(omega：50, m：200, n：1000, cost, A, epsilon：0.1, params["isAuto"], params["isZBin"]; timeLimits=timeLimits, gap=0.0)
function OriModel(omega, m, n, cost, A, epsilon, isAuto, isZBin; timeLimits=7200, gap=0.0)
    model = SeleteSolver(solver)

    # 设置模型求解时间限制和求解 gap
    set_time_limit_sec(model, timeLimits)
    set_optimizer_attribute(model, "CPXPARAM_MIP_Tolerances_MIPGap", gap)

    # 创建 0-1 变量 x
    @variable(model, x[j in 1:n], Bin)

    # 设置线程数为 1
    MOI.set(model, MOI.NumberOfThreads(), 1)

    # 是否调用 AutoBenders 求解
    if isAuto
        # 创建变量 z 为连续变量
        @variable(model, 0 <= z[w in 1:omega] <= 1)
        #@variable(model, 0 <= z[w in 1:omega, i in 1:m] <= 1)
        # 设置参数 CPXPARAM_Benders_Strategy 为 3
        set_optimizer_attribute(model, "CPXPARAM_Benders_Strategy", 3)
    else
        if isZBin
            # 创建变量 z 为 0-1 变量
            @variable(model, z[w in 1:omega], Bin)
            #@variable(model, z[w in 1:omega, i in 1:m], Bin)
        else
            # 创建变量 z 为连续变量
            @variable(model, 0 <= z[w in 1:omega] <= 1)
            #@variable(model, 0 <= z[w in 1:omega, i in 1:m] <= 1)
        end
    end

    # 目标函数: 最小化 c^T x
    @objective(model, Min, cost' * x)
    
    # 创建约束
    @constraint(model, linearcons[i in 1:m, w in 1:omega], sum(x[A[w][i]]) >= z[w])
    @constraint(model, probabilitycons, sum(z[w] for w in 1:omega) >= ceil((1-epsilon)*omega))
    # 求解模型
    optimize!(model)

    # 输出相关求解信息
    PrintInfos(model)
    PrintInfosBD(model)

    # write_to_file(model, "model.lp")
end
