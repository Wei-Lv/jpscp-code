#julia ./jpscp-code/JPSCP/src/JPSCP.jl ./jpscp-code/JPSCP/scp41-50-0.gz 50 0 0 0.1 1 1 720 isHeur=false isStren=false isPre=false isMIR=false isInit=false isAuto=false isZBin=false
include("Auxiliary.jl")
include("preprocess.jl")
include("Basic_Cuts.jl")

function BendersModel(omega, m, n, cost, A, numA, epsilon; isPrint=true, isPre=false, isOri=true, gap=0.0, timelimits=7200, prefix="main")
#BendersModel(omega::Int32, m::Int32, n::Int32, cost::Vector{Int32}, A::Vector{Dict{Int64, Vector{Int64}}}, numA::Matrix{Int64}, epsilon::Float64;
# isHeur::Bool, isPrint::Bool, isStren::Bool, isMIR::Bool, isPre::Bool, isInit::Bool(布尔型参数), FixedSites::Nothing(不包含任何值得类型), isOri::Bool, gap::Float64, timelimits::Int64, prefix::String)

    model = SeleteSolver(solver)
    # set timelimits, gap
    set_time_limit_sec(model, timelimits)
    set_optimizer_attribute(model, "CPXPARAM_MIP_Tolerances_MIPGap", gap)

    # bulid 0-1 variables x
    @variable(model, x[j in 1:n], Bin)
    # objective function: minimize c^T x
    @objective(model, Min, cost' * x)

    start = time()

    # scaling
    ScaledP = ones(Int, omega, m)
    ScaledEps = floor(Int, epsilon * omega) #ScaledEps::Int

    # presolving
    DelOmega, BasicCons, ConsCoef, isSatisfied = nothing, nothing, zeros(m), zeros(Int8, m)
    KeepOmega = [[o for o in 1:omega] for i in 1:m]#m维数组，每个元素是1：Omega的数组
    if isOri && isPre
        DelOmega, KeepOmega, BasicCons, ConsCoef, isSatisfied, _, = propagation3(m, omega, ScaledP, ScaledEps, A, numA)
        println("Number of Satisfied: ", sum(isSatisfied))
    end

    # statistics presolving time
    presolvetime = time()-start

    # random matrix column storage
    AbyColumn = ComputeAbyColumn(A, numA, omega, m, n, isPre, DelOmega)
    UnsatisfiedM = [i for i in 1:m if isSatisfied[i] == 0]

    # count the number of add cons
    nFeaconslazy = 0
    nFeaconsuser = 0
    nStrConsuser = 0
    nMIRConsuser = 0
    # statistics time spent on lazy and user processes
    timeBDLazy = 0.
    timeBDUser = 0.
    # statistics cut information
    cutInfo = Dict("lazy" => 0., "user" => 0., "nlazy" => 0, "nuser" => 0, "mir"=>0.0)
    # record node information
    NodeInfo = Dict("LastDepth" => 0, "nCall" => 0, "LastNode" => 0)

    """
    Call the callback function of Cplex to dynamically add the violated feasibility constraint
    Constraints required to ensure that the model is solved correctly.
    Add Lazy Constraint, Used to eliminate some infeasible integer solutions.
    """
    function LazyConstraint(cb_data)
        lazyConsTimeStart = time()

        nodecount = Ref{CPXLONG}()
        status = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODECOUNT, nodecount)
        @assert status == 0
        
        nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 1, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
        nFeaconslazy += nCurFeaCons

        timeBDLazy += (time()-lazyConsTimeStart)
    end
    MOI.set(model, MOI.LazyConstraintCallback(), LazyConstraint)

    """
    Cutting out some non-integer solutions, Compact model and accelerate problem solving
    """
    function UserCut(cb_data)

        userCutsTimeStart = time()

        # get the current node number 
        nodecount = Ref{CPXLONG}()
        status = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODECOUNT, nodecount)

        if nodecount[] == NodeInfo["LastNode"]
            NodeInfo["nCall"] += 1
        else
            NodeInfo["LastNode"] = nodecount[]
            NodeInfo["nCall"] = 1
        end

        # get the current node depth 
        nodedepth = Ref{CPXLONG}()
        res = CPXcallbackgetinfolong(cb_data, CPXCALLBACKINFO_NODEDEPTH, nodedepth)

        # add User cut restriction: (i) only two entries are allowed at child nodes (ii) depth frequency is 10
        if (nodecount[] < 1 || NodeInfo["nCall"] <= 2) && (nodedepth[] <= 10 || nodedepth[] == NodeInfo["LastDepth"] + 10)
            # record node depth
            NodeInfo["LastDepth"] = nodedepth[]
            
            if rootorchild == 0
                if nodecount[] == 0
                    nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                    nFeaconsuser += nCurFeaCons
                end
            else
                nCurFeaCons = FeaCons(cb_data, model, x, m, n, omega, ScaledP, A, ScaledEps, AbyColumn, 0, cutInfo, nodecount[], UnsatisfiedM, KeepOmega, ConsCoef)
                nFeaconsuser += nCurFeaCons
            end
        end
        
        timeBDUser += (time() - userCutsTimeStart)
    end
    MOI.set(model, MOI.UserCutCallback(), UserCut)

    optimize!(model)
    if isPrint
        # solving information
        PrintInfos(model; prefix=prefix)
        PrintInfosBD(model; prefix=prefix)

        # presolving information
        @printf("[%s] %s %.2f\n", prefix, "Presolving time: ", presolvetime)

        # adding cut number information
        @printf("[%s] %s %d\n", prefix, "Number of basci_cons lazy Cons: ", nFeaconslazy)
        @printf("[%s] %s %d\n", prefix, "Number of basci_cons user Cons: ", nFeaconsuser)

        @printf("[%s] %s %d\n", prefix, "Number of str_mir user Cons: ", nStrConsuser+nMIRConsuser)
        
        # add cut time information
        @printf("[%s] %s %.2f\n", prefix, "MIR spent time: ", cutInfo["mir"])
        @printf("[%s] %s %.2f %s %d\n", prefix, "Lazy func time: ", cutInfo["lazy"], "Lazy func num: ", cutInfo["nlazy"])
        @printf("[%s] %s %.2f %s %d\n", prefix, "User func time: ", cutInfo["user"], "User func num: ", cutInfo["nuser"])
        @printf("[%s] %s %.2f\n", prefix, "AddBDCuts time: ", timeBDUser+timeBDLazy)
    end

    # get the optimal solution and value for heuristic
    pstatus = primal_status(model)
    sol_x = []
    objvalue = Inf
    if pstatus == FEASIBLE_POINT
        sol_x = value.(x)
        objvalue = objective_value(model)
    end

    return pstatus, sol_x, objvalue
end
