import MathOptInterface
const MOI = MathOptInterface

using JuMP
using CPLEX
using Printf

function SeleteSolver(solver)
    if solver == "Cplex"
        return direct_model(CPLEX.Optimizer())
    else
        println("please selete a solver!")
    end
end

function PrintInfos(model; prefix="main")
    # (OPTIMAL/TIME_LIMIT/INFEASIBLE)

    # @show termination_status(model)
    terstatus = termination_status(model)
    @printf("[%s] %s %s\n", prefix, "termination_status: ", terstatus)

    #  (FEASIBLE_POINT/NO_SOLUTION)
    pstatus = primal_status(model)
    @printf("[%s] %s %s\n", prefix, "primal_status: ", pstatus)

    # Output solution information 
    if termination_status(model) == OPTIMAL
        # round(optimal value)
        opt_objective = round(objective_value(model))
        # @show opt_objective
        @printf("[%s] %s %.2f\n", prefix, "objective_value: ", opt_objective)
    elseif termination_status(model) == TIME_LIMIT
        # println("opt_objective = -1")
        opt_objective = round(objective_value(model))
        @printf("[%s] %s %.2f\n", prefix, "objective_value: ", opt_objective)
    else
        if termination_status(model) != INFEASIBLE
            error("The model was not solved correctly !!!")
        end
    end
    
    # number of nodes
    node = node_count(model)
    @printf("[%s] %s %d\n", prefix, "node_count: ", node)

    # solve time
    # @show solve_time(model)
    solvetime = solve_time(model)
    @printf("[%s] %s %.2f\n", prefix, "solve_time:", solvetime)
end

function PrintInfosBD(model; prefix="main")
   cplex_model = backend(model)
   env, lp = cplex_model.env, cplex_model.lp
   gaps = Ref{Cdouble}(0.0)
   status = CPXgetmiprelgap(env, lp, gaps)

   @printf("[%s] %s %.2f\n", prefix, "Current gap: ", round(gaps[]*100; digits=2))
   
   #    itcnt = CPXgetmipitcnt(env, lp)
   #    println("Itcnt: ", itcnt)
end
